﻿//
// SignaturePad.cs: User Control subclass for Windows Phone to allow users to draw their signature on 
// the device to be captured as an image or vector.
//
// Author:
//   Timothy Risi (timothy.risi@gmail.com)
//
// Copyright (C) 2012 Timothy Risi
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;

namespace Xamarin.Controls
{
    public partial class SignaturePad : UserControl
    {
        private Point previousPosition = default(Point);
        private List<List<Point>> points = new List<List<Point>>();
        private bool pressed = false;
        private BitmapInfo bitmapInfo = new BitmapInfo();

        //Create an array containing all of the points used to draw the signature.  Uses (0, 0)
        //to indicate a new line.
        public Point[] Points
        {
            get
            {
                if (points == null || points.Count() == 0)
                {
                    return new Point[0];
                }

                IEnumerable<Point> pointsList = points[0];

                for (var i = 1; i < points.Count; i++)
                {
                    pointsList = pointsList.Concat(new[] { new Point(0, 0) });
                    pointsList = pointsList.Concat(points[i]);
                }

                return pointsList.ToArray();
            }
        }

        public bool IsBlank
        {
            get { return points == null || points.Count() == 0 || !(points.Where(p => p.Any()).Any()); }
        }

        /// <summary>
        /// Gets or sets the color of the strokes for the signature.
        /// </summary>
        /// <value>The color of the stroke.</value>
        public SolidColorBrush Stroke
        {
            get; private set;
        }

        public Color StrokeColor
        {
            get { return Stroke.Color; }
            set
            {
                Stroke.Color = value;
                if (!IsBlank)
                {
                    UpdateBitmapBuffer();
                }
            }
        }

        private Color backgroundColor;
        public Color BackgroundColor
        {
            get { return backgroundColor; }
            set
            {
                backgroundColor = value;
                LayoutRoot.Background = new SolidColorBrush(value);
            }
        }

        /// <summary>
        /// Gets or sets the width in pixels of the strokes for the signature.
        /// </summary>
        /// <value>The width of the line.</value>
        public float StrokeWidth
        {
            get { return (int)GetValue(StrokeWidthProperty); }
            set { SetValue(StrokeWidthProperty, (int)value); }
        }

        public static readonly DependencyProperty StrokeWidthProperty =
            DependencyProperty.Register("StrokeWidth", typeof(int), typeof(SignaturePad), new PropertyMetadata(2));

        /// <summary>
        /// The caption displayed under the signature line.
        /// </summary>
        /// <remarks>
        /// Text value defaults to 'Sign here.'
        /// </remarks>
        /// <value>The caption.</value>
        public TextBlock Caption
        {
            get { return caption; }
        }

        public string CaptionText
        {
            get { return caption.Text; }
            set { caption.Text = value; }
        }

        public TextBlock ClearLabel
        {
            get { return clearText; }
        }

        public string ClearLabelText
        {
            get { return clearText.Text; }
            set { clearText.Text = value; }
        }

        /// <summary>
        /// The prompt displayed at the beginning of the signature line.
        /// </summary>
        /// <remarks>
        /// Text value defaults to 'X'.
        /// </remarks>
        /// <value>The signature prompt.</value>
        public TextBlock SignaturePrompt
        {
            get { return signaturePrompt; }
        }

        public string SignaturePromptText
        {
            get { return signaturePrompt.Text; }
            set { signaturePrompt.Text = value; }
        }

        /// <summary>
        /// The color of the signature line.
        /// </summary>
        /// <value>The color of the signature line.</value>
        private Color signatureLineColor;
        public Color SignatureLineColor
        {
            get { return signatureLineColor; }
            set
            {
                signatureLineColor = value;
                signatureLine.BorderBrush = new SolidColorBrush(value);
            }
        }

        public SignaturePad()
        {
            InitializeComponent();

            points = new List<List<Point>>();
            Stroke = new SolidColorBrush(Colors.White);
            BackgroundColor = Colors.Black;
            StrokeWidth = 3;
            SizeChanged += SignaturePad_SizeChanged;
        }

        /// <summary>
        /// Fires when SignatureBox has been loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void inkPresenter_Loaded(object sender, RoutedEventArgs e)
        {
            var background = base.Background ?? new SolidColorBrush(Colors.White);
            Background = background;
        }

        private async void SignaturePad_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            await UpdateImage();
        }

        public void Clear()
        {
            var lines = new List<Line>();

            foreach (var child in inkPresenter.Children)
            {
                if (child.GetType() == typeof(Line))
                {
                    lines.Add(child as Line);
                }
            }

            foreach (var line in lines)
            {
                inkPresenter.Children.Remove(line);
            }

            points = new List<List<Point>>();

            clearText.Visibility = Visibility.Collapsed;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }


        #region Image access and updates
        private WriteableBitmap CurrentUncroppedImage { get; set; }

        public WriteableBitmap GetImage()
        {
            return CurrentUncroppedImage;
        }

        private async Task UpdateImage()
        {
            Size size = RenderSize;

            if (bitmapInfo.BitmapBuffer == null)
            {
                return;
            }
            if (size.Width == 0 || size.Height == 0)
            {
                return;
            }

            InMemoryRandomAccessStream ramStream = new InMemoryRandomAccessStream();

            BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, ramStream);
            encoder.SetPixelData(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        (uint)bitmapInfo.PixelWidth,
                        (uint)bitmapInfo.PixelHeight, 96d, 96d,
                        bitmapInfo.BitmapBuffer.ToArray());
            await encoder.FlushAsync();
            ramStream.Seek(0);

            WriteableBitmap uncroppedBitmap = new WriteableBitmap(bitmapInfo.PixelWidth, bitmapInfo.PixelHeight);
            await uncroppedBitmap.SetSourceAsync(ramStream);
            CurrentUncroppedImage = uncroppedBitmap;
        }
        #endregion


        #region Touch Events
        /// <summary>
        /// Handles the pointer pressed event. Sets the previousPosition to the 
        /// current position of the pointer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void inkPresenter_OnPointerPressed(object sender, PointerRoutedEventArgs e)
        {
            previousPosition = e.GetCurrentPoint(this).Position;
            pressed = true;
        }

        /// <summary>
        /// Handles the pointer moved event. If the pointer is pressed and has moved it will 
        /// draw the line on the canvas and add the points drawn to the points list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void inkPresenter_OnPointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (!pressed)
            {
                return;
            }

            var positions = e.GetIntermediatePoints(this).Select(ppt => ppt.Position);
            var currentPosition = e.GetCurrentPoint(this).Position;

            // Only add points if previous and current positions are different
            if (positions.Any() &&
                currentPosition.X != previousPosition.X &&
                currentPosition.Y != previousPosition.Y)
            {
                points.Add(new List<Point>());
                points.Last().Add(previousPosition);

                foreach (Point pt in positions)
                {
                    inkPresenter.Children.Add(new Line()
                    {
                        X1 = previousPosition.X,
                        Y1 = previousPosition.Y,
                        X2 = pt.X,
                        Y2 = pt.Y,
                        Stroke = Stroke,
                        StrokeThickness = StrokeWidth
                    });
                    previousPosition = pt;
                }
                points.Last().AddRange(positions);
            }
        }

        /// <summary>
        /// Handles the PointerReleased Event. Toggled the Visibility of the clearText TextBlock
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected async void inkPresenter_OnPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            pressed = false;

            if (inkPresenter.Children.Any())
            {
                clearText.Visibility = Visibility.Visible;
            }

            await UpdateBitmapBuffer();
        }
#endregion

        // Allow the user to import an array of points to be used to draw a signature in the view, with new
        // lines indicated by a PointF.Empty in the array.
        public async void LoadPoints(Point[] loadedPoints)
#warning FIXME : convert API to "async Task" ...
        {
            if (!loadedPoints.Any())
            {
                return;
            }

            points = new List<List<Point>>();

            foreach (Point pt in loadedPoints)
            {
                inkPresenter.Children.Add(new Line()
                {
                    X1 = previousPosition.X,
                    Y1 = previousPosition.Y,
                    X2 = pt.X,
                    Y2 = pt.Y,
                    Stroke = Stroke,
                    StrokeThickness = StrokeWidth
                });
                previousPosition = pt;
            }

            points.Last().AddRange(loadedPoints);

            clearText.Visibility = Visibility.Visible;

            await UpdateBitmapBuffer();
        }

        /// <summary>
        /// Updates the BitmapBuffer object with the current state of the canvas
        /// </summary>
        private async Task UpdateBitmapBuffer()
        {
            Size canvasSize = inkPresenter.RenderSize;
            Point defaultPoint = inkPresenter.RenderTransformOrigin;

            inkPresenter.Measure(canvasSize);
            inkPresenter.UpdateLayout();
            inkPresenter.Arrange(new Rect(defaultPoint, canvasSize));

            var renderTargetBitmap = new RenderTargetBitmap();
            await renderTargetBitmap.RenderAsync(inkPresenter);

            bitmapInfo.BitmapBuffer = await renderTargetBitmap.GetPixelsAsync();
            bitmapInfo.PixelWidth = renderTargetBitmap.PixelWidth;
            bitmapInfo.PixelHeight = renderTargetBitmap.PixelHeight;

            await UpdateImage();
        }

        /// <summary>
        /// Class to contain the Current State of the Canvas Bitmap
        /// </summary>
        private class BitmapInfo
        {
            public IBuffer BitmapBuffer
            {
                get; set;
            }
            public int PixelWidth
            {
                get; set;
            }
            public int PixelHeight
            {
                get; set;
            }
        }
    }
}
